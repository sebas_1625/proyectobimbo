//
//  Cliente.swift
//  SimpleTableView
//
//  Created by Sebastián Loredo on 05/09/16.
//  Copyright © 2016 Sebastián Loredo. All rights reserved.
//

import Foundation
import UIKit

class Cliente: UIViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet var tableView: UITableView!
    
    var opcion: Int = 0
    var LabelText = ""
    var campo = ""
    var items: [String] = ["Ubicar Tiendas", "Mini juegos", "Mis Cupones", "Quejas y Sugerencias"]
    var Datos = ""
    @IBOutlet weak var saludo: UILabel!
    
    override func viewDidLoad() {
        let value = UIInterfaceOrientation.portrait.rawValue
        UIDevice.current.setValue(value, forKey: "orientation")
        super.viewDidLoad()
        saludo.text = "Hola " + Datos
        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        print("Hello Cliente")
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.items.count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:UITableViewCell = self.tableView.dequeueReusableCell(withIdentifier: "cell")! as UITableViewCell
        
        cell.textLabel?.text = self.items[(indexPath as NSIndexPath).row]
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if (indexPath as NSIndexPath).row == 0 {
            let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
            let destination = storyboard.instantiateViewController(withIdentifier: "Sencillo") as! Sencillo
            destination.LabelText = "Ubicar Tiendas"
            navigationController?.pushViewController(destination, animated: true)
        } else if (indexPath as NSIndexPath).row == 1 {
            let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
            let destination = storyboard.instantiateViewController(withIdentifier: "Juego")
            navigationController?.pushViewController(destination, animated: true)
        } else if (indexPath as NSIndexPath).row == 2 {
            let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
            let destination = storyboard.instantiateViewController(withIdentifier: "Cupones")
            navigationController?.pushViewController(destination, animated: true)
        } else {
            let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
            let destination = storyboard.instantiateViewController(withIdentifier: "Comentarios")
            navigationController?.pushViewController(destination, animated: true)
        }
    }
    
    override var shouldAutorotate : Bool {
        return true
    }
}
