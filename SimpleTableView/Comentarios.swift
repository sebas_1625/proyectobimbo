//
//  Comentarios.swift
//  SimpleTableView
//
//  Created by Sebastián Loredo on 07/10/16.
//  Copyright © 2016 Sebastián Loredo. All rights reserved.
//

import Foundation
import UIKit
import Firebase

class Comentarios: UIViewController {
    
    @IBOutlet weak var `switch`: UISwitch!
    //@IBOutlet weak var si: UILabel!
    @IBOutlet weak var nombreTienda: UITextField!
    @IBOutlet weak var comentario: UITextField!
    var Datos = ""
    var LabelText = ""
    var si = "Si"
    
    override func viewDidLoad() {
        self.navigationController?.isNavigationBarHidden = false
        `switch`.setOn(true, animated:true)
        
        `switch`.addTarget(self, action: #selector(Comentarios.stateChanged(_:)), for: UIControlEvents.valueChanged)

        super.viewDidLoad()
    }
    
    func stateChanged(_ switchState: UISwitch) {
        if switchState.isOn {
            si = "Si"
        } else {
            si = "No"
        }
    }
    
    @IBOutlet weak var enviar: UIButton!
    @IBAction func enviar(_ sender: AnyObject) {
        if nombreTienda.text!.isEmpty || comentario.text!.isEmpty {
            let alert = UIAlertView()
            alert.title = "Campo Vacío"
            alert.message = "Debes dejar algún comentario o queja."
            alert.addButton(withTitle: "Ok")
            alert.show()
        } else {
            
            if(FIRApp.defaultApp() == nil){
                FIRApp.configure()
            }
        
            FIRAuth.auth()!.addStateDidChangeListener() { (auth, user) in
                if let user = user {
                    print("User is signed in with uid:", user.uid)
                }
            }
        
            let rootRef = FIRDatabase.database().reference()
            var anonimo = ""
            var correo = ""

            if self.si == "Si" {
                anonimo = "Anónimo"
                correo = "Anónimo"
            } else {
                anonimo = String(self.Datos)
                correo = String(self.LabelText)
            }
            
            let itemDic: [String: String] = [
                "nombreTienda": String(self.nombreTienda.text!),
                "comentario": String(self.comentario.text!),
                "nombre": anonimo,
                "correo": correo,
            ]
            rootRef.child("comentarios").childByAutoId().setValue(itemDic)
            
            let alert = UIAlertView()
            alert.title = "Mensaje enviado"
            alert.addButton(withTitle: "Ok")
            alert.show()
            enviar.isEnabled = false
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        nombreTienda.resignFirstResponder()
        comentario.resignFirstResponder()
    }
}
