//
//  Cupones.swift
//  SimpleTableView
//
//  Created by Sebastián Loredo on 20/09/16.
//  Copyright © 2016 Sebastián Loredo. All rights reserved.
//

import Foundation
import UIKit
import MultipeerConnectivity
import CoreLocation
import Firebase

class Cupones: UIViewController, UITableViewDelegate, UITableViewDataSource, MCBrowserViewControllerDelegate, MCSessionDelegate, CLLocationManagerDelegate {
    
    @IBOutlet var tableView: UITableView!
    
    var opcion: Int = 0
    var campo = ""
    var varPrueba = ""
    var locationManager = CLLocationManager()
    var items: [String] = []
    var arrDispositivos: NSMutableArray = NSMutableArray()
    var sesion: MCSession!
    var variablePrueba = ""
    var Correo = ""
    var anunciante:  MCAdvertiserAssistant! // -> difundir datos del dispositivo
    var browser: MCBrowserViewController! // -> muestra dispositvos cercanos para poder conectarnos
    var peerID: MCPeerID! // datos del dispositivo (nombre), el anunciante ocupará el PeerID
    
    override func viewDidLoad() {
        self.navigationController?.isNavigationBarHidden = false
        super.viewDidLoad()
        locationManager = CLLocationManager()
        locationManager.delegate = self;
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        locationManager.startUpdatingLocation()
        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        self.configurarSesion()
        
        if(FIRApp.defaultApp() == nil){
            FIRApp.configure()
        }
        
        FIRAuth.auth()!.addStateDidChangeListener() { (auth, user) in
            if let user = user {
                print("User is signed in with uid:", user.uid)
            }
        }
        
        let rootRef = FIRDatabase.database().reference()
        
        let userRef = rootRef.child("clientes")
        
        userRef.observe(.childAdded, with: {(snapshot) in
            if snapshot.exists() {
                let currency = (snapshot.value as? NSDictionary)
                
                let correoCliente = currency?["correo"] as! String
                if (correoCliente == self.Correo) {
                    let promociones = currency?["cupones"] as? String ?? ""
                    let arrPromociones = promociones.characters.split{$0 == ","}.map(String.init)
                    self.items = arrPromociones
                    self.tableView.reloadData()
                }
            }
        })
    }
    
    private func locationManager(manager: CLLocationManager!, didUpdateLocations locations: [AnyObject]!) {
        let userLocation:CLLocation = locations[0] as! CLLocation
        let long = userLocation.coordinate.longitude;
        let lat = userLocation.coordinate.latitude;
        print(long)
        print(lat)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.items.count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:UITableViewCell = self.tableView.dequeueReusableCell(withIdentifier: "cell")! as UITableViewCell
        cell.textLabel?.text = self.items[(indexPath as NSIndexPath).row]
        cell.backgroundColor = UIColor.clear
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let destination = storyboard.instantiateViewController(withIdentifier: "TiendasAmigas") as! TiendasAmigas
        destination.Cupon = self.items[(indexPath as NSIndexPath).row]
        destination.Correo = self.Correo
        self.navigationController?.pushViewController(destination, animated: true)
    }
    
    @IBAction func conectar(_ sender: AnyObject) {
        self.browser = MCBrowserViewController(serviceType: "chat-proyecto", session: self.sesion)
        self.browser.delegate = self
        self.present(self.browser, animated: true, completion: nil)
    }
    
    @IBAction func desconectar(_ sender: AnyObject) {
        if arrDispositivos.count == 0 {
            
        } else {
            self.anunciante.stop() // detiene difusion de datos
            self.anunciante.delegate = nil
            self.sesion.disconnect() // Se desconecta
            self.sesion.delegate = nil
            self.arrDispositivos.removeAllObjects()
            self.configurarSesion() // Reinicia
            let alert = UIAlertView()
            alert.title = "Éxito"
            alert.message = "Desconexión satisfactoria"
            alert.addButton(withTitle: "Ok")
            alert.show()
        }
    }
    
    
    func yaConectados() {
        
        let datosMensaje = String(varPrueba).data(using: String.Encoding.utf8)
        
        let peersConectados = self.sesion.connectedPeers
        
        do {
            try self.sesion.send(datosMensaje!, toPeers: peersConectados, with: MCSessionSendDataMode.reliable)
        } catch {
            print("Error al enviar los datos!! :(")
        }
        
        //let datosMensaje = self.tfMensaje.text?.dataUsingEncoding(NSUTF8StringEncoding)
        //print(datosMensaje)
        
    }
    fileprivate func configurarSesion() {
        
        // Crear el peerID con el nombre del dispositivo
        self.peerID = MCPeerID(displayName: UIDevice.current.name)
        
        // Crear el objeto que mantiene la sesión
        self.sesion = MCSession(peer: self.peerID) //
        self.sesion.delegate = self
        
        // Anunciante que difunde los datos del teléfono
        self.anunciante = MCAdvertiserAssistant(serviceType: "chat-proyecto", discoveryInfo: nil, session: self.sesion)
        self.anunciante.start()
    }
    
    // MARK: - Métodos del browser
    
    func browserViewControllerDidFinish(_ browserViewController: MCBrowserViewController) {
        self.browser.dismiss(animated: true, completion: nil)
    }
    
    func browserViewControllerWasCancelled(_ browserViewController: MCBrowserViewController) {
        self.browser.dismiss(animated: true, completion: nil)
    }
    
    
    // MARK: - Métodos de la sesión
    
    func session(_ session: MCSession, peer peerID: MCPeerID, didChange state: MCSessionState) {
        // Conecta/desconecta/cancela
        let nombrePeer = peerID.displayName
        
        switch state {
        case MCSessionState.connecting:
            print("Conectado con: \(nombrePeer)")
        case MCSessionState.notConnected:
            print("Se desconectó \(nombrePeer)")
        case MCSessionState.connected:
            print("Conexión establecida con \(nombrePeer)")
            self.arrDispositivos.add(nombrePeer)
            print("Conectados: \(self.arrDispositivos)")
        }
        
    }
    
    func session(_ session: MCSession, didReceive data: Data, fromPeer peerID: MCPeerID) {
        // Llegan datos de un peer
        let mensajeRecibido = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
        print("Datos recibidos: \(mensajeRecibido!)")
        /*
         let btn = self.arrBotones![mensajeRecibido!]
         
         dispatch_async(dispatch_get_main_queue()) { () -> Void in
         
         btn.setTitle("1", forState: UIControlState.Normal)
         
         }
         verificar()
         */
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        print("Recibido")
        if (opcion == 0){
            //let DestViewController: Sencillo = segue.destinationViewController as! Sencillo
            //DestViewController.LabelText = "Sencillo"
            //let destination = Sencillo() // Your destination
            //navigationController?.pushViewController(destination, animated: true)
        }
        if (opcion == 1){
            //let DestViewController: Nivel2 = segue.destinationViewController as! Nivel2
            //DestViewController.LabelText = lblCampo.text!
            print("Opcion 2")
        }
        if (opcion == 2){
            //let DestViewController: Nivel3 = segue.destinationViewController as! Nivel3
            //DestViewController.LabelText = lblCampo.text!
            print("Opcion 3")
        }
    }
    
    
    // Métodos no utilizados
    
    func session(_ session: MCSession, didReceive stream: InputStream, withName streamName: String, fromPeer peerID: MCPeerID) {
        //
    }
    
    func session(_ session: MCSession, didStartReceivingResourceWithName resourceName: String, fromPeer peerID: MCPeerID, with progress: Progress) {
        //
    }
    
    func session(_ session: MCSession, didFinishReceivingResourceWithName resourceName: String, fromPeer peerID: MCPeerID, at localURL: URL, withError error: Error?) {
        //
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
