//
//  Entrar
//  SimpleTableView
//
//  Created by Sebastián Loredo on 15/09/16.
//  Copyright © 2016 Sebastián Loredo. All rights reserved.
//

import Foundation
import UIKit
import Firebase
import SystemConfiguration

class Entrar: UIViewController {
    
    @IBOutlet weak var tfCorreo: UITextField!
    @IBOutlet weak var tfContra: UITextField!
    var opcion = 0
    var myArray = [String]()
    var nombre = ""
    var num = 0
    
    override func viewDidLoad() {
        let value = UIInterfaceOrientation.portrait.rawValue
        UIDevice.current.setValue(value, forKey: "orientation")
        super.viewDidLoad()
    }
    
    @IBAction func entrarMenu(_ sender: AnyObject) {
        
        if(FIRApp.defaultApp() == nil){
            FIRApp.configure()
        }
        
        FIRAuth.auth()!.addStateDidChangeListener() { (auth, user) in
            if let user = user {
                print("User is signed in with uid:", user.uid)
            }
        }
        
        let rootRef = FIRDatabase.database().reference()
        
        if isValidEmail(tfCorreo.text!) && tfContra.text! != "" {
            let userRef = rootRef.child("clientes")
            
            userRef.queryOrdered(byChild: "correo").queryEqual(toValue: tfCorreo.text!).observe(.value, with: { snapshot in
                if snapshot.exists() {
                    let currency = snapshot.value! as! NSDictionary
                    var contra = ""
                    var nombre = ""
                    var correo = ""
                    for (_, value) in currency {
                        let value2 = value as! NSDictionary
                        for (key, value) in value2 {
                            if key as! String == "contra" {
                                contra = value as! String
                            }
                            if key as! String == "nombre" {
                                nombre = value as! String
                            }
                            if key as! String == "correo" {
                                correo = value as! String
                            }
                        }
                    }
                    
                    if contra == self.tfContra.text! {
                        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
                        let destination = storyboard.instantiateViewController(withIdentifier: "Main") as! Main
                        destination.Datos = nombre
                        destination.Correo = correo
                        destination.LabelText = self.tfCorreo.text!
                        self.navigationController?.pushViewController(destination, animated: true)
                    } else {
                        self.error()
                    }
                } else if !snapshot.exists(){
                    self.error()
                }
            })
        } else if isValidEmail(tfCorreo.text!) && tfContra.text! == "" {
            error()
        } else if tfCorreo.text! == "" && tfContra.text! == "" {
            error()
        } else {
            let userRef = rootRef.child("Tiendas")
            
            userRef.observe(.childAdded, with: {(snapshot) in
                if snapshot.exists() {
                    let currency = (snapshot.value as? NSDictionary)
                    
                    var codigo = currency?["codigo"] as! Int
                    var nombre = currency?["nombre"] as! String
                    if (codigo == Int(self.tfCorreo.text!)) {
                        
                        self.num = 1
                        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
                        let destination = storyboard.instantiateViewController(withIdentifier: "Tendero") as! Tendero
                        destination.Nombre = nombre
                        destination.Codigo = codigo
                        self.navigationController?.pushViewController(destination, animated: true)
                    }
                } else {
                    self.error()
                }
            })
        }
    }
    
    func isValidEmail(_ testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    func error() {
        let alert = UIAlertView()
        alert.title = "Error"
        alert.message = "Usuario o contraseña inválidos"
        alert.addButton(withTitle: "Ok")
        alert.show()
    }
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        tfCorreo.resignFirstResponder()
        tfContra.resignFirstResponder()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        self.view.endEditing(true)
        
        return true
        
    }
    
    override var shouldAutorotate : Bool {
        return true
    }
}
