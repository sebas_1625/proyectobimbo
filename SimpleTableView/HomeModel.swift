import Foundation
import UIKit
import Firebase

protocol HomeModelProtocal: class {
    func itemsDownloaded(_ items: NSArray)
}


class HomeModel: NSObject, URLSessionDataDelegate {
    
    //properties
    weak var delegate: HomeModelProtocal!
    var data: NSMutableData = NSMutableData()
    
    func downloadItems(_ LabelText: String) {
        if LabelText == "Sencillo" {
            download("https://github.com/Sebastianilli/test/blob/master/sencillo.txt", LabelText: LabelText)
        } else if LabelText == "Elaborado" {
            download("https://github.com/Sebastianilli/test/blob/master/elaborado.txt", LabelText: LabelText)
        } else {
            download("https://github.com/Sebastianilli/test/blob/master/tema.txt", LabelText: LabelText)
        }
    }
    
    func download(_ url: String, LabelText: String) {
        if let url = URL(string: url) {
            do {
                let contents = try NSString(contentsOf: url, usedEncoding: nil)
                //print(contents)
                self.parseJSON(contents, LabelText: LabelText)
            } catch {
                print("contents could not be loaded")
            }
        } else {
            print("the URL was bad!")
        }
    }
    
    func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, didReceive data: Data) {
        self.data.append(data);
    }
    
    func urlSession(_ session: URLSession, task: URLSessionTask, didCompleteWithError error: Error?) {
        if error != nil {
            print("Failed to download data")
        }else {
            print("Data downloaded")
        }
    }
    
    func parseJSON(_ contents :NSString, LabelText: String) {
        var items: [String] = ["Ubicar Tiendas", "Mini juegos", "Por Tema"]
        
        if LabelText == "Ubicar Tiendas" {
            
            var myArray = [String]()
            var latitudes = [String]()
            var longitudes = [String]()
            
            if(FIRApp.defaultApp() == nil){
                FIRApp.configure()
            }
            
            FIRAuth.auth()!.addStateDidChangeListener() { (auth, user) in
                if let user = user {
                    print("User is signed in with uid:", user.uid)
                }
            }
            
            let rootRef = FIRDatabase.database().reference()
            
            rootRef.queryOrdered(byChild: "Tiendas").observeSingleEvent(of: .value, with: { (snapshot) in
                
                if snapshot.exists() {
                    
                    var a = [String]()
                    
                    for i in (1...56) {
                        a.append(i.description)
                    }
                    
                    let userRef = rootRef.child("Tiendas")
                    
                    userRef.observe(.childAdded, with: {(snapshot) in
                        let currency = (snapshot.value as? NSDictionary)
                        let nombre = currency!["nombre"]
                        let lat = currency!["latitud"]
                        let lon = currency!["longitud"]
                        
                        let nombre2 = nombre as! String
                        let lat2 = (lat! as AnyObject).description
                        let lon2 = (lon! as AnyObject).description
                        
                        myArray.append(nombre2)
                        latitudes.append(lat2!)
                        longitudes.append(lon2!)
                        
                        let locations: NSMutableArray = NSMutableArray()
                        
                        for i in 0 ..< myArray.count {
                            
                            let plat = myArray[i]
                            let lat = latitudes[i]
                            let lon = longitudes[i]
                            
                            let location = LocationModel()
                            location.platillo = plat
                            location.cantidad = lat
                            location.precio = lon
                            locations.add(location)
                        }
                        
                        DispatchQueue.main.async(execute: { () -> Void in
                            
                            self.delegate.itemsDownloaded(locations)
                            
                        })
                    })
                    
                }
            })
        } else if LabelText == "Mini juegos" {
            
        }
    }
}
