//
//  Juego.swift
//  SimpleTableView
//
//  Created by Sebastián Loredo on 20/09/16.
//  Copyright © 2016 Sebastián Loredo. All rights reserved.
//

import Foundation
import UIKit
import Firebase

class Juego: UIViewController {
    
    @IBOutlet weak var btn1: UIButton!
    @IBOutlet weak var btn2: UIButton!
    @IBOutlet weak var btn3: UIButton!
    @IBOutlet weak var btn4: UIButton!
    @IBOutlet weak var btn5: UIButton!
    @IBOutlet weak var btn6: UIButton!
    @IBOutlet weak var btn7: UIButton!
    var producto: [String] = []
    var promocion: [String] = []
    var Correo = ""
    
    @IBAction func cambiarDigito1(_ sender: AnyObject) {
        
        if(sender.titleLabel!!.text == "1") {
            btn1.setImage(UIImage(named: "nito1.png"), for: UIControlState())
            btn1.setTitle("0", for: UIControlState())
            
            if(btn2.titleLabel!.text == "1") {
                btn2.setImage(UIImage(named: "nito1.png"), for: UIControlState())
                btn2.setTitle("0", for: UIControlState())
            } else {
                btn2.setImage(UIImage(named: "osito.png"), for: UIControlState())
                btn2.setTitle("1", for: UIControlState())
            }
        } else {
            btn1.setImage(UIImage(named: "osito.png"), for: UIControlState())
            btn1.setTitle("1", for: UIControlState())
            
            if(btn2.titleLabel!.text == "1") {
                btn2.setImage(UIImage(named: "nito1.png"), for: UIControlState())
                btn2.setTitle("0", for: UIControlState())
            } else {
                btn2.setImage(UIImage(named: "osito.png"), for: UIControlState())
                btn2.setTitle("1", for: UIControlState())
            }
        }
        verificarGanar()
    }
    
    @IBAction func cambiarDigito2(_ sender: AnyObject) {
        if(sender.titleLabel!!.text == "1") {
            btn2.setImage(UIImage(named: "nito1.png"), for: UIControlState())
            btn2.setTitle("0", for: UIControlState())
            
            if(btn1.titleLabel!.text == "1") {
                btn1.setImage(UIImage(named: "nito1.png"), for: UIControlState())
                btn1.setTitle("0", for: UIControlState())
            } else {
                btn1.setImage(UIImage(named: "osito.png"), for: UIControlState())
                btn1.setTitle("1", for: UIControlState())
            }
            
            if(btn3.titleLabel!.text == "1") {
                btn3.setImage(UIImage(named: "nito1.png"), for: UIControlState())
                btn3.setTitle("0", for: UIControlState())
            } else {
                btn3.setImage(UIImage(named: "osito.png"), for: UIControlState())
                btn3.setTitle("1", for: UIControlState())
            }
        } else {
            btn2.setImage(UIImage(named: "osito.png"), for: UIControlState())
            btn2.setTitle("1", for: UIControlState())
            
            if(btn1.titleLabel!.text == "1") {
                btn1.setImage(UIImage(named: "nito1.png"), for: UIControlState())
                btn1.setTitle("0", for: UIControlState())
            } else {
                btn1.setImage(UIImage(named: "osito.png"), for: UIControlState())
                btn1.setTitle("1", for: UIControlState())
            }
            
            if(btn3.titleLabel!.text == "1") {
                btn3.setImage(UIImage(named: "nito1.png"), for: UIControlState())
                btn3.setTitle("0", for: UIControlState())
            } else {
                btn3.setImage(UIImage(named: "osito.png"), for: UIControlState())
                btn3.setTitle("1", for: UIControlState())
            }
        }
        verificarGanar()
    }
    
    @IBAction func cambiarDigito3(_ sender: AnyObject) {
        if(sender.titleLabel!!.text == "1") {
            btn3.setImage(UIImage(named: "nito1.png"), for: UIControlState())
            btn3.setTitle("0", for: UIControlState())
            
            if(btn2.titleLabel!.text == "1") {
                btn2.setImage(UIImage(named: "nito1.png"), for: UIControlState())
                btn2.setTitle("0", for: UIControlState())
            } else {
                btn2.setImage(UIImage(named: "osito.png"), for: UIControlState())
                btn2.setTitle("1", for: UIControlState())
            }
            
            if(btn4.titleLabel!.text == "1") {
                btn4.setImage(UIImage(named: "nito1.png"), for: UIControlState())
                btn4.setTitle("0", for: UIControlState())
            } else {
                btn4.setImage(UIImage(named: "osito.png"), for: UIControlState())
                btn4.setTitle("1", for: UIControlState())
            }
        } else {
            btn3.setImage(UIImage(named: "osito.png"), for: UIControlState())
            btn3.setTitle("1", for: UIControlState())
            
            if(btn2.titleLabel!.text == "1") {
                btn2.setImage(UIImage(named: "nito1.png"), for: UIControlState())
                btn2.setTitle("0", for: UIControlState())
                
            } else {
                btn2.setImage(UIImage(named: "osito.png"), for: UIControlState())
                btn2.setTitle("1", for: UIControlState())
            }
            
            if(btn4.titleLabel!.text == "1") {
                btn4.setImage(UIImage(named: "nito1.png"), for: UIControlState())
                btn4.setTitle("0", for: UIControlState())
            } else {
                btn4.setImage(UIImage(named: "osito.png"), for: UIControlState())
                btn4.setTitle("1", for: UIControlState())
            }
        }
        verificarGanar()
    }
    
    @IBAction func cambiarDigito4(_ sender: AnyObject) {
        if(sender.titleLabel!!.text == "1") {
            btn4.setImage(UIImage(named: "nito1.png"), for: UIControlState())
            btn4.setTitle("0", for: UIControlState())
            
            if(btn3.titleLabel!.text == "1") {
                btn3.setImage(UIImage(named: "nito1.png"), for: UIControlState())
                btn3.setTitle("0", for: UIControlState())
            } else {
                btn3.setImage(UIImage(named: "osito.png"), for: UIControlState())
                btn3.setTitle("1", for: UIControlState())
            }
            
            if(btn5.titleLabel!.text == "1") {
                btn5.setImage(UIImage(named: "nito1.png"), for: UIControlState())
                btn5.setTitle("0", for: UIControlState())
            } else {
                btn5.setImage(UIImage(named: "osito.png"), for: UIControlState())
                btn5.setTitle("1", for: UIControlState())
            }
        } else {
            btn4.setImage(UIImage(named: "osito.png"), for: UIControlState())
            btn4.setTitle("1", for: UIControlState())
            
            if(btn3.titleLabel!.text == "1") {
                btn3.setImage(UIImage(named: "nito1.png"), for: UIControlState())
                btn3.setTitle("0", for: UIControlState())
            } else {
                btn3.setImage(UIImage(named: "osito.png"), for: UIControlState())
                btn3.setTitle("1", for: UIControlState())
            }
            
            if(btn5.titleLabel!.text == "1") {
                btn5.setImage(UIImage(named: "nito1.png"), for: UIControlState())
                btn5.setTitle("0", for: UIControlState())
            } else {
                btn5.setImage(UIImage(named: "osito.png"), for: UIControlState())
                btn5.setTitle("1", for: UIControlState())
            }
        }
        verificarGanar()
    }
    
    @IBAction func cambiarDigito5(_ sender: AnyObject) {
        if(sender.titleLabel!!.text == "1") {
            btn5.setImage(UIImage(named: "nito1.png"), for: UIControlState())
            btn5.setTitle("0", for: UIControlState())
            
            if(btn4.titleLabel!.text == "1") {
                btn4.setImage(UIImage(named: "nito1.png"), for: UIControlState())
                btn4.setTitle("0", for: UIControlState())
            } else {
                btn4.setImage(UIImage(named: "osito.png"), for: UIControlState())
                btn4.setTitle("1", for: UIControlState())
            }
            
            if(btn6.titleLabel!.text == "1") {
                btn6.setImage(UIImage(named: "nito1.png"), for: UIControlState())
                btn6.setTitle("0", for: UIControlState())
            } else {
                btn6.setImage(UIImage(named: "osito.png"), for: UIControlState())
                btn6.setTitle("1", for: UIControlState())
            }
        } else {
            btn5.setImage(UIImage(named: "osito.png"), for: UIControlState())
            btn5.setTitle("1", for: UIControlState())
            
            if(btn4.titleLabel!.text == "1") {
                btn4.setImage(UIImage(named: "nito1.png"), for: UIControlState())
                btn4.setTitle("0", for: UIControlState())
            } else {
                btn4.setImage(UIImage(named: "osito.png"), for: UIControlState())
                btn4.setTitle("1", for: UIControlState())
            }
            
            if(btn6.titleLabel!.text == "1") {
                btn6.setImage(UIImage(named: "nito1.png"), for: UIControlState())
                btn6.setTitle("0", for: UIControlState())
            } else {
                btn6.setImage(UIImage(named: "osito.png"), for: UIControlState())
                btn6.setTitle("1", for: UIControlState())
            }
        }
        verificarGanar()
    }
    
    @IBAction func cambiarDigito6(_ sender: AnyObject) {
        if(sender.titleLabel!!.text == "1") {
            btn6.setImage(UIImage(named: "nito1.png"), for: UIControlState())
            btn6.setTitle("0", for: UIControlState())
            
            if(btn5.titleLabel!.text == "1") {
                btn5.setImage(UIImage(named: "nito1.png"), for: UIControlState())
                btn5.setTitle("0", for: UIControlState())
            } else {
                btn5.setImage(UIImage(named: "osito.png"), for: UIControlState())
                btn5.setTitle("1", for: UIControlState())
            }
        } else {
            btn6.setImage(UIImage(named: "osito.png"), for: UIControlState())
            btn6.setTitle("1", for: UIControlState())
            
            if(btn5.titleLabel!.text == "1") {
                btn5.setImage(UIImage(named: "nito1.png"), for: UIControlState())
                btn5.setTitle("0", for: UIControlState())
            } else {
                btn5.setImage(UIImage(named: "osito.png"), for: UIControlState())
                btn5.setTitle("1", for: UIControlState())
            }
        }
        verificarGanar()
    }

    func verificarGanar() {
        if (btn6.titleLabel!.text == "1" &&
            btn5.titleLabel!.text == "1" &&
            btn4.titleLabel!.text == "1" &&
            btn3.titleLabel!.text == "1" &&
            btn2.titleLabel!.text == "1" &&
            btn1.titleLabel!.text == "1") {
            
            if(FIRApp.defaultApp() == nil){
                FIRApp.configure()
            }
            
            FIRAuth.auth()!.addStateDidChangeListener() { (auth, user) in
                if let user = user {
                    print("User is signed in with uid:", user.uid)
                }
            }
            
            let rootRef = FIRDatabase.database().reference()
            
            let userRef = rootRef.child("promociones")
            
            userRef.queryOrdered(byChild: "producto").observe(.value, with: { snapshot in
                if snapshot.exists() {
                    let currency = snapshot.value! as! NSDictionary
                    
                    for (_, value) in currency {
                        let value2 = value as! NSDictionary
                        for (key, value) in value2 {
                            if key as! String == "producto" {
                                self.producto.append(value as! String)
                            } else {
                                self.promocion.append(value as! String)
                            }
                        }
                    }
                    let randomIndex = Int(arc4random_uniform(UInt32(self.producto.count)))
                    var prod = self.producto[randomIndex]
                    var prom = self.promocion[randomIndex]
                    var pp = prod + prom
                    
                    let userRef2 = rootRef.child("clientes")
                    var nombres = [String]()
                    
                    userRef2.observe(.childAdded, with: {(snapshot) in
                        
                        if snapshot.exists() {
                            let key = snapshot.key
                            let currency = snapshot.value! as! NSDictionary
                            var correo = currency["correo"] as? String ?? ""
                            
                            if (correo == self.Correo) {
                                
                                var cupones = currency["cupones"] as? String ?? ""
                                cupones = cupones + pp
                                
                                let arrPromociones = cupones.characters.split{$0 == ","}.map(String.init)
                                
                                var text = ""
                                
                                for i in arrPromociones {
                                    text = text + i + ","
                                }
                                
                                print(text)
                                rootRef.child("clientes").child(key).updateChildValues(["cupones": text])
                                let alert = UIAlertView()
                                alert.title = "Listo"
                                alert.message = "Ganaste " + pp
                                alert.addButton(withTitle: "Ok")
                                alert.show()
                            }
                        }
                    })
                }
            })
        } else if (btn6.titleLabel!.text == "0" &&
            btn5.titleLabel!.text == "0" &&
            btn4.titleLabel!.text == "0" &&
            btn3.titleLabel!.text == "0" &&
            btn2.titleLabel!.text == "0" &&
            btn1.titleLabel!.text == "0") {
            
            
            if(FIRApp.defaultApp() == nil){
                FIRApp.configure()
            }
            
            FIRAuth.auth()!.addStateDidChangeListener() { (auth, user) in
                if let user = user {
                    print("User is signed in with uid:", user.uid)
                }
            }
            
            let rootRef = FIRDatabase.database().reference()
            
            let userRef = rootRef.child("promociones")
            
            userRef.queryOrdered(byChild: "producto").observe(.value, with: { snapshot in
                if snapshot.exists() {
                    let currency = snapshot.value! as! NSDictionary
                    
                    for (_, value) in currency {
                        let value2 = value as! NSDictionary
                        for (key, value) in value2 {
                            if key as! String == "producto" {
                                self.producto.append(value as! String)
                            } else {
                                self.promocion.append(value as! String)
                            }
                        }
                    }
                    let randomIndex = Int(arc4random_uniform(UInt32(self.producto.count)))
                    var prod = self.producto[randomIndex]
                    var prom = self.promocion[randomIndex]
                    var pp = prod + prom
                    
                    let userRef2 = rootRef.child("clientes")
                    var nombres = [String]()
                    
                    userRef2.observe(.childAdded, with: {(snapshot) in
                        
                        if snapshot.exists() {
                            let key = snapshot.key
                            let currency = snapshot.value! as! NSDictionary
                            var correo = currency["correo"] as? String ?? ""
                            
                            if (correo == self.Correo) {
                                
                                var cupones = currency["cupones"] as? String ?? ""
                                cupones = cupones + pp
                                
                                let arrPromociones = cupones.characters.split{$0 == ","}.map(String.init)
                                
                                var text = ""
                                
                                for i in arrPromociones {
                                    text = text + i + ","
                                }
                                
                                print(text)
                                rootRef.child("clientes").child(key).updateChildValues(["cupones": text])
                                let alert = UIAlertView()
                                alert.title = "Listo"
                                alert.message = "Ganaste " + pp
                                alert.addButton(withTitle: "Ok")
                                alert.show()
                            }
                        }
                    })
                    
                }
            })
        }
    }
    
    override func viewDidLoad() {
        self.navigationController?.isNavigationBarHidden = false
        super.viewDidLoad()
    }
    
}
