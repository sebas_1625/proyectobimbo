import Foundation
 
class LocationModel: NSObject {
    
    //properties
    var platillo: String?
    var cantidad: String?
    var precio: String?
    
    //empty constructor
    override init() {}
    
    init(platillo: String, cantidad: String, precio: String) {
        self.platillo = platillo
        self.cantidad = cantidad
        self.precio = precio
    }
    
    //prints object's current state
    override var description: String {
        return "Platillo: \(platillo), Cantidad: \(cantidad), Precio: \(precio)"
    }
}