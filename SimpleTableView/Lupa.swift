import Foundation


class Lupa {
    
    var juegoGanado: Bool = false
    var juegoIniciado: Bool = false
    var btn1: Int = 0
    var btn2: Int = 0
    var btn3: Int = 0
    var btn4: Int = 0
    var btn5: Int = 0
    var btn6: Int = 0
    var btn7: Int = 0
    var tiros: Int = 0
    
    
    func comenzarJuego() {
        
        juegoIniciado = true
        juegoGanado = false
        tiros = 0
        
        btn1 = Int(arc4random()%2)
        btn2 = Int(arc4random()%2)
        btn3 = Int(arc4random()%2)
        btn4 = Int(arc4random()%2)
        btn5 = Int(arc4random()%2)
        btn6 = Int(arc4random()%2)
        btn7 = Int(arc4random()%2)
    }
    
    
    func cambiarEstadoNumeros(boton: Int) {
        
        if (juegoIniciado == true) {
            
            switch(boton) {
                
            case 1:
                if (btn1 == 1) {
                    btn1 = 0
                } else {
                    btn1 = 1
                }
                
                if (btn2 == 1) {
                    btn2 = 0
                } else {
                    btn2 = 1
                }
                
            case 2:
                if (btn2 == 1) {
                    btn2 = 0
                } else {
                    btn2 = 1
                }
                
                if (btn1 == 1) {
                    btn1 = 0
                } else {
                    btn1 = 1
                }
                
                if (btn3 == 1) {
                    btn3 = 0
                } else {
                    btn3 = 1
                }
                
            case 3:
                if (btn3 == 1) {
                    btn3 = 0
                } else {
                    btn3 = 1
                }
                
                if (btn2 == 1) {
                    btn2 = 0
                } else {
                    btn2 = 1
                }
                
                if (btn4 == 1) {
                    btn4 = 0
                } else {
                    btn4 = 1
                }
                
            case 4:
                if (btn4 == 1) {
                    btn4 = 0
                } else {
                    btn4 = 1
                }
                
                if (btn3 == 1) {
                    btn3 = 0
                } else {
                    btn3 = 1
                }
                
                if (btn5 == 1) {
                    btn5 = 0
                } else {
                    btn5 = 1
                }
                
            case 5:
                if (btn5 == 1) {
                    btn5 = 0
                } else {
                    btn5 = 1
                }
                
                if (btn4 == 1) {
                    btn4 = 0
                } else {
                    btn4 = 1
                }
                
                if (btn6 == 1) {
                    btn6 = 0
                } else {
                    btn6 = 1
                }
                
            case 6:
                if (btn6 == 1) {
                    btn6 = 0
                } else {
                    btn6 = 1
                }
                
                if (btn5 == 1) {
                    btn5 = 0
                } else {
                    btn5 = 1
                }
                
                if (btn7 == 1) {
                    btn7 = 0
                } else {
                    btn7 = 1
                }
                
            case 7:
                if (btn7 == 1) {
                    btn7 = 0
                } else {
                    btn7 = 1
                }
                
                if (btn6 == 1) {
                    btn6 = 0
                } else {
                    btn6 = 1
                }
                
            default:
                "holi"
                
            }
            
            tiros++
        }
    }
    
    func ganaste()->Bool {
        
        if (btn1 == 1 && btn2 == 1 && btn3 == 1 && btn4 == 1 && btn5 == 1 && btn6 == 1 && btn7 == 1 ||
            btn1 == 0 && btn2 == 0 && btn3 == 0 && btn4 == 0 && btn5 == 0 && btn6 == 0 && btn7 == 0) {
            
            juegoIniciado = false
            juegoGanado = true
            
            return true
        } else {
            return false
        }
        
    }
}