//
//  Main.swift
//  SimpleTableView
//
//  Created by Sebastián Loredo on 05/09/16.
//  Copyright © 2016 Sebastián Loredo. All rights reserved.
//

import Foundation
import UIKit
import Firebase

class Main: UIViewController {
    @IBOutlet var tableView: UITableView!
    
    var opcion: Int = 0
    var campo = ""
    var items: [String] = ["Ubicar Tiendas", "Mini juegos", "Mis Cupones", "Quejas y Sugerencias"]
    var Datos = ""
    var Correo = ""
    var LabelText = ""
    var arrPromociones:[String] = []
    
    @IBOutlet weak var mensajes: UIButton!
    @IBOutlet weak var saludo: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        saludo.text = Datos
        
        if(FIRApp.defaultApp() == nil){
            FIRApp.configure()
        }
        
        FIRAuth.auth()!.addStateDidChangeListener() { (auth, user) in
            if let user = user {
                print("User is signed in with uid:", user.uid)
            }
        }
        
        let rootRef = FIRDatabase.database().reference()
        
        let userRef = rootRef.child("clientes")
        
        userRef.observe(.childAdded, with: {(snapshot) in
            if snapshot.exists() {
                let currency = (snapshot.value as? NSDictionary)
                
                let correo = currency?["correo"] as! String
                if (correo == self.Correo) {
                    let promociones = currency?["mensajesTendero"] as? String ?? ""
                    let arrPromociones = promociones.characters.split{$0 == ","}.map(String.init)
                    self.arrPromociones = arrPromociones
                    
                    if arrPromociones.count > 0 {
                        let image = UIImage(named: "msg1.png") as UIImage?
                        self.mensajes.setImage(image, for: .normal)
                    }
                }
            }
        })
    }

    @IBAction func ubicarTiendas(_ sender: AnyObject) {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let destination = storyboard.instantiateViewController(withIdentifier: "Sencillo") as! Sencillo
        destination.LabelText = "Ubicar Tiendas"
        destination.Correo = self.Correo
        navigationController?.pushViewController(destination, animated: true)
    }
    
    @IBAction func miniJuegos(_ sender: AnyObject) {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let destination = storyboard.instantiateViewController(withIdentifier: "Juego") as! Juego
        destination.Correo = self.Correo
        navigationController?.pushViewController(destination, animated: true)
    }
    
    @IBAction func misCupones(_ sender: AnyObject) {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let destination = storyboard.instantiateViewController(withIdentifier: "Cupones") as! Cupones
        destination.Correo = self.Correo
        navigationController?.pushViewController(destination, animated: true)
    }
    
    @IBAction func contacto(_ sender: AnyObject) {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let destination = storyboard.instantiateViewController(withIdentifier: "Comentarios") as! Comentarios
        destination.Datos = Datos
        destination.LabelText = LabelText
        navigationController?.pushViewController(destination, animated: true)
    }
    
    @IBAction func mensajes(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let destination = storyboard.instantiateViewController(withIdentifier: "MisMensajes") as! MisMensajes
        destination.Datos = Datos
        destination.Correo = self.Correo
        navigationController?.pushViewController(destination, animated: true)
    }
    
    override var shouldAutorotate : Bool {
        return true
    }
}
