//
//  MensajeClientes.swift
//  SimpleTableView
//
//  Created by Sebastián Loredo on 06/11/16.
//  Copyright © 2016 Sebastián Loredo. All rights reserved.
//

import Foundation
import UIKit
import Firebase

class MensajeClientes: UIViewController {
    
    @IBOutlet weak var mensaje: UITextField!
    @IBOutlet weak var enviar: UIButton!
    
    var Codigo:Int = 0
    var Nombre = ""
    var timer = Timer()
    var arrPromociones: [String] = []
    var arrSolicitudesCupon: [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        scheduledTimerWithTimeInterval()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        mensaje.resignFirstResponder()
    }
    
    @IBAction func enviar(_ sender: Any) {
        if mensaje.text!.isEmpty {
            let alert = UIAlertView()
            alert.title = "Campo Vacío"
            alert.addButton(withTitle: "Ok")
            alert.show()
        } else {
            
            if(FIRApp.defaultApp() == nil){
                FIRApp.configure()
            }
        
            FIRAuth.auth()!.addStateDidChangeListener() { (auth, user) in
                if let user = user {
                    print("User is signed in with uid:", user.uid)
                }
            }
        
            let rootRef = FIRDatabase.database().reference()
            
            let userRef = rootRef.child("Tiendas")
        
            userRef.observe(.childAdded, with: {(snapshot) in
                if snapshot.exists() {
                
                    let currency = (snapshot.value as? NSDictionary)
                    let codigo = currency?["codigo"] as! Int
                    if (codigo == self.Codigo) {
                        let promociones = currency?["amigos"] as? String ?? ""
                        let arrPromociones = promociones.characters.split{$0 == ","}.map(String.init)
                    
                        var dict = [String:Int]()
                    
                        for i in arrPromociones {
                            let holi = i.characters.split{$0 == ":"}.map(String.init)
                            dict[holi[0]] = Int(holi[1])!
                        }
                    
                        let keyValueArray = dict.valueKeySorted
                    
                        var newArray: [String] = []
                    
                        for (key,_) in keyValueArray {
                            newArray.append(key)
                        }
                        let first5 = Array(newArray.prefix(10))

                        let userRef1 = rootRef.child("clientes")
                    
                        userRef1.observe(.childAdded, with: {(snapshot) in
                            if snapshot.exists() {
                                let currency = (snapshot.value as? NSDictionary)
                                let correo = currency?["correo"] as! String
                                let key = snapshot.key
                                
                                let mensajes = currency?["mensajesTendero"] as! String
                                
                                let date = Date()
                                let calendar = Calendar.current
                                
                                let hour = String(calendar.component(.hour, from: date))
                                let minutes = String(calendar.component(.minute, from: date))
                                let seconds = String(calendar.component(.second, from: date))
                                
                                let year = calendar.component(.year, from: date)
                                let month = calendar.component(.month, from: date)
                                let day = calendar.component(.day, from: date)
                                
                                let fecha = String(day) + "/" + String(month) + "/" + String(year)
                                let hora = hour + ":" + minutes + ":" + seconds
                                
                                for i in first5 {
                                    if i == correo {
                                        let text = mensajes + String(self.Nombre) + ";" + fecha + ";" + hora + ";" + self.mensaje.text! + ","
                                        
                                        rootRef.child("clientes").child(key).updateChildValues(["mensajesTendero": text])
                                    }
                                }
                            }
                        })

                    }
                }
            })
            
            let alert = UIAlertView()
            alert.title = "Mensaje Enviado a tus clientes leales"
            alert.addButton(withTitle: "Ok")
            alert.show()
            
            enviar.isEnabled = false
        }
    }
    
    func scheduledTimerWithTimeInterval() {
        // Scheduling timer to Call the function **Countdown** with the interval of 1 seconds
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.revisarSolicitud), userInfo: nil, repeats: true)
    }
    
    func revisarSolicitud() {
        
        if(FIRApp.defaultApp() == nil){
            FIRApp.configure()
        }
        
        FIRAuth.auth()!.addStateDidChangeListener() { (auth, user) in
            if let user = user {
                print("User is signed in with uid:", user.uid)
            }
        }
        
        let rootRef = FIRDatabase.database().reference()
        
        let userRef = rootRef.child("Tiendas")
        
        userRef.observe(.childAdded, with: {(snapshot) in
            if snapshot.exists() {
                let currency = (snapshot.value as? NSDictionary) //316641
                let codigo = currency?["codigo"] as! Int
                let key = snapshot.key
                
                if (codigo == self.Codigo) {
                    
                    let solicitudes = currency?["solicitudes"] as? String ?? ""
                    
                    self.arrPromociones = solicitudes.characters.split{$0 == ","}.map(String.init)
                    
                    if self.arrPromociones.count > 0 {
                        let refreshAlert = UIAlertController(title: "Refresh", message: "Aceptar la solicitud?", preferredStyle: UIAlertControllerStyle.alert)
                        
                        refreshAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
                            
                            var amigos = currency!["amigos"] as? String ?? ""
                            amigos = amigos + self.arrPromociones[0]
                            
                            let arrPromociones1 = amigos.characters.split{$0 == ","}.map(String.init)
                            
                            var listaNueva: [String] = []
                            
                            for i in arrPromociones1 {
                                if listaNueva.contains(i) {
                                    
                                } else {
                                    listaNueva.append(i)
                                }
                            }
                            
                            var text = ""
                            
                            for i in listaNueva {
                                text = text + i + ","
                            }
                            
                            rootRef.child("Tiendas").child(key).updateChildValues(["amigos": text])
                            
                            let newText = ""
                            
                            rootRef.child("Tiendas").child(key).updateChildValues(["solicitudes": newText])
                            
                            let userRef = rootRef.child("clientes")
                            
                            userRef.observe(.childAdded, with: {(snapshot) in
                                if snapshot.exists() {
                                    let currency = (snapshot.value as? NSDictionary) //316641
                                    let correo = currency?["correo"] as! String
                                    let key = snapshot.key
                                    let arrPromociones1 = amigos.characters.split{$0 == ","}.map(String.init)
                                    
                                    if (correo == arrPromociones1[0]) {
                                        
                                        var amigos = currency!["amigos"] as? String ?? ""
                                        amigos = amigos + String(self.Codigo)
                                        
                                        let arrPromociones2 = amigos.characters.split{$0 == ","}.map(String.init)
                                        
                                        var listaNueva: [String] = []
                                        
                                        for i in arrPromociones2 {
                                            if listaNueva.contains(i) {
                                                
                                            } else {
                                                listaNueva.append(i)
                                            }
                                        }
                                        
                                        var text = ""
                                        
                                        for i in listaNueva {
                                            text = text + i + ","
                                        }
                                        
                                        rootRef.child("clientes").child(key).updateChildValues(["amigos": text])
                                    }
                                }
                            })
                            self.scheduledTimerWithTimeInterval()
                            
                        }))
                        
                        refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action: UIAlertAction!) in
                            let newText = ""
                            
                            rootRef.child("Tiendas").child(key).updateChildValues(["solicitudes": newText])
                            self.scheduledTimerWithTimeInterval()
                            
                        }))
                        
                        self.present(refreshAlert, animated: true, completion: nil)
                    }
                }
            }
        })
        
        let userRef2 = rootRef.child("Tiendas")
        
        userRef2.observe(.childAdded, with: {(snapshot) in
            if snapshot.exists() {
                let currency = (snapshot.value as? NSDictionary) //316641
                let codigo = currency?["codigo"] as! Int
                let key = snapshot.key
                
                if (codigo == self.Codigo) {
                    
                    let solicitudes = currency?["solicitudesCupon"] as? String ?? ""
                    
                    self.arrSolicitudesCupon = solicitudes.characters.split{$0 == ","}.map(String.init)
                    
                    if self.arrSolicitudesCupon.count > 0 {
                        let refreshAlert = UIAlertController(title: "Refresh", message: "Aceptar la solicitud?", preferredStyle: UIAlertControllerStyle.alert)
                        
                        refreshAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
                            
                            var cuponesCanjeados = currency!["cuponesCanjeados"] as? String ?? ""
                            cuponesCanjeados = cuponesCanjeados + self.arrSolicitudesCupon[0]
                            
                            let arrPromociones1 = cuponesCanjeados.characters.split{$0 == ","}.map(String.init)
                            
                            print(arrPromociones1)
                            let arrDesgloce = arrPromociones1.last?.characters.split{$0 == ":"}.map(String.init)
                            
                            var text = ""
                            print(arrDesgloce)
                            
                            
                            for i in arrPromociones1 {
                                text = text + i.characters.split{$0 == ":"}.map(String.init)[0] + ","
                            }
                            
                            rootRef.child("Tiendas").child(key).updateChildValues(["cuponesCanjeados": text])
                            
                            let newText = ""
                            
                            rootRef.child("Tiendas").child(key).updateChildValues(["solicitudesCupon": newText])
                            
                            let userRef = rootRef.child("clientes")
                            
                            userRef.observe(.childAdded, with: {(snapshot) in
                                if snapshot.exists() {
                                    let currency = (snapshot.value as? NSDictionary) //316641
                                    let correo = currency?["correo"] as! String
                                    let key = snapshot.key
                                    
                                    if (correo == arrDesgloce?[1]) {
                                        
                                        var cupones = currency!["cupones"] as? String ?? ""
                                        
                                        var arrPromociones2 = cupones.characters.split{$0 == ","}.map(String.init)
                                        
                                        var listaNueva: [String] = []
                                        
                                        if let index = arrPromociones2.index(of: arrDesgloce![0]) {
                                            arrPromociones2.remove(at: index)
                                        }
                                        
                                        var text = ""
                                        
                                        for i in arrPromociones2 {
                                            text = text + i + ","
                                        }
                                        
                                        rootRef.child("clientes").child(key).updateChildValues(["cupones": text])
                                        
                                    }
                                }
                            })
                            
                            self.scheduledTimerWithTimeInterval()
                            
                        }))
                        
                        refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action: UIAlertAction!) in
                            let newText = ""
                            
                            rootRef.child("Tiendas").child(key).updateChildValues(["solicitudesCupon": newText])
                            self.scheduledTimerWithTimeInterval()
                            
                        }))
                        
                        self.present(refreshAlert, animated: true, completion: nil)
                    }
                }
            }
        })
    }
}
