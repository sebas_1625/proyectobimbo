//
//  MisMensajes.swift
//  SimpleTableView
//
//  Created by Sebastián Loredo on 06/11/16.
//  Copyright © 2016 Sebastián Loredo. All rights reserved.
//

import Foundation
import UIKit
import Firebase

class MisMensajes: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    var items = [String]()
    var tendero:[String] = []
    var fecha:[String] = []
    var hora:[String] = []
    var mensaje:[String] = []
    var index = 0
    var Codigo:Int = 0
    var Datos = ""
    var Correo = ""
    var timer = Timer()
    var arrPromociones: [String] = []
    var arrSolicitudesCupon: [String] = []
    
    @IBOutlet weak var lblDe: UILabel!
    @IBOutlet weak var lblEl: UILabel!
    @IBOutlet weak var lblMensaje: UILabel!
    
    override func viewDidLoad() {
        tableView.delegate = self
        tableView.dataSource = self
        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        
        if(FIRApp.defaultApp() == nil){
            FIRApp.configure()
        }
        
        FIRAuth.auth()!.addStateDidChangeListener() { (auth, user) in
            if let user = user {
                print("User is signed in with uid:", user.uid)
            }
        }
        
        let rootRef = FIRDatabase.database().reference()
        
        let userRef = rootRef.child("clientes")
        
        userRef.observe(.childAdded, with: {(snapshot) in
            if snapshot.exists() {
                let currency = (snapshot.value as? NSDictionary)
                
                let correo = currency?["correo"] as! String
                if (correo == self.Correo) {
                    let promociones = currency?["mensajesTendero"] as? String ?? ""
                    let arrPromociones = promociones.characters.split{$0 == ","}.map(String.init)
                    var tendero:[String] = []
                    var fecha:[String] = []
                    var hora:[String] = []
                    var mensaje:[String] = []

                    for i in arrPromociones {
                        let y = i.characters.split{$0 == ";"}.map(String.init)
                        tendero.append(y[0])
                        fecha.append(y[1])
                        hora.append(y[2])
                        mensaje.append(y[3])
                    }
                    self.tendero = tendero
                    self.fecha = fecha
                    self.hora = hora
                    self.mensaje = mensaje
                    self.items = mensaje
                    self.arrPromociones = arrPromociones
                    self.tableView.reloadData()
                }
            }
        })
        
        super.viewDidLoad()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.items.count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:UITableViewCell = self.tableView.dequeueReusableCell(withIdentifier: "cell")! as UITableViewCell
        
        cell.textLabel?.text = self.items[(indexPath as NSIndexPath).row]
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        index = (indexPath as NSIndexPath).row
        lblDe.text = tendero[index]
        lblEl.text = "El: " + fecha[index] + " a la(s): " + hora[index]
        lblMensaje.text = mensaje[index]
        borrar.isEnabled = true
    }
    
    
    @IBOutlet weak var borrar: UIButton!
    @IBAction func borrarMensaje(_ sender: Any) {
        if lblMensaje.text == "" {
            borrar.isEnabled = false
        } else {
            var arrTemp:[String] = []
            
            for i in arrPromociones {
                if i == arrPromociones[index] {
                    
                } else {
                    arrTemp.append(i)
                }
            }
            
            var text = ""
            
            for i in arrTemp {
                text = text + i + ","
            }
            
            if(FIRApp.defaultApp() == nil){
                FIRApp.configure()
            }
            
            FIRAuth.auth()!.addStateDidChangeListener() { (auth, user) in
                if let user = user {
                    print("User is signed in with uid:", user.uid)
                }
            }
            
            let rootRef = FIRDatabase.database().reference()
            
            let userRef = rootRef.child("clientes")
            
            userRef.observe(.childAdded, with: {(snapshot) in
                if snapshot.exists() {
                    let currency = (snapshot.value as? NSDictionary)
                    let key = snapshot.key
                    let correoCliente = currency?["correo"] as! String
                    
                    if (correoCliente == self.Correo) {
                        rootRef.child("clientes").child(key).updateChildValues(["mensajesTendero": text])

                        let arrPromociones = text.characters.split{$0 == ","}.map(String.init)
                        var tendero:[String] = []
                        var fecha:[String] = []
                        var hora:[String] = []
                        var mensaje:[String] = []
                        
                        for i in arrPromociones {
                            let y = i.characters.split{$0 == ";"}.map(String.init)
                            tendero.append(y[0])
                            fecha.append(y[1])
                            hora.append(y[2])
                            mensaje.append(y[3])
                        }
                        self.tendero = tendero
                        self.fecha = fecha
                        self.hora = hora
                        self.mensaje = mensaje
                        self.items = mensaje
                        self.arrPromociones = arrPromociones
                        self.lblDe.text = ""
                        self.lblEl.text = ""
                        self.lblMensaje.text = ""
                    }
                }
            })
        }
    }
}
