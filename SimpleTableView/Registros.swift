//
//  Registros.swift
//  SimpleTableView
//
//  Created by Sebastián Loredo on 10/09/16.
//  Copyright © 2016 Sebastián Loredo. All rights reserved.
//

import Foundation
import UIKit
import Firebase

class Registros: UIViewController {
    
    @IBOutlet weak var tfNombre: UITextField!
    @IBOutlet weak var tfCorreo: UITextField!
    @IBOutlet weak var tfContra: UITextField!
    @IBOutlet weak var tfDireccion: UITextField!
    @IBOutlet weak var btnRegistrarse: UIButton!
    
    var numero = 0
    
    var myArray = [String]()
    var mailArray = [String]()
    
    var libre = 0
    
    override func viewDidLoad() {
    }
    
    @IBAction func Registro(_ sender: AnyObject) {
        
        if tfNombre.text!.isEmpty || tfCorreo.text!.isEmpty || tfCorreo.text!.isEmpty || tfDireccion.text!.isEmpty {
            let alert = UIAlertView()
            alert.title = "Campo Vacío"
            alert.message = "Ninguna sección puede quedar vacía"
            alert.addButton(withTitle: "Ok")
            alert.show()
        } else {
            
            if(FIRApp.defaultApp() == nil){
                FIRApp.configure()
            }
            
            FIRAuth.auth()!.addStateDidChangeListener() { (auth, user) in
                if let user = user {
                    print("User is signed in with uid:", user.uid)
                }
            }
            
            let rootRef = FIRDatabase.database().reference()
            
            let userRef = rootRef.child("clientes")
            
            userRef.queryOrdered(byChild: "correo").queryEqual(toValue: tfCorreo.text!).observe(.value, with: { snapshot in
                if snapshot.exists() == false {
                    let itemDic: [String: String] = [
                        "correo": String(self.tfCorreo.text!),
                        "nombre" : String(self.tfNombre.text!),
                        "direccion": String(self.tfDireccion.text!),
                        "contra": String(self.tfContra.text!),
                        "amigos": "",
                        "cupones": "",
                        "mensajesTendero": ""
                    ]
                    rootRef.child("clientes").childByAutoId().setValue(itemDic)
                    
                    /*
                     let alert = UIAlertView()
                     alert.title = "Usuario registrado"
                     alert.addButton(withTitle: "Ok")
                     alert.show()
                     */
                    
                    let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
                    let destination = storyboard.instantiateViewController(withIdentifier: "Entrar")
                    self.navigationController?.pushViewController(destination, animated: true)
                } else {
                    /*
                     let alert = UIAlertView()
                     alert.title = "Error"
                     alert.message = "Correo ya registrado"
                     alert.addButton(withTitle: "Ok")
                     alert.show()
                     */
                }
            })
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        tfCorreo.resignFirstResponder()
        tfContra.resignFirstResponder()
        tfNombre.resignFirstResponder()
        tfDireccion.resignFirstResponder()
    }
}
