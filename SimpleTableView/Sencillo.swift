import Foundation
import UIKit
import CoreLocation
import MapKit
import Firebase

class CustomPointAnnotation: MKPointAnnotation {
    var imageName: String!
}

class Sencillo: UIViewController, CLLocationManagerDelegate, MKMapViewDelegate {
    
    var feedItems: NSArray = NSArray()
    var selectedLocation : LocationModel = LocationModel()
    var LabelText: String?
    var Correo = ""
    
    //Mapa
    @IBOutlet weak var mapa: MKMapView!
    var matchingItems: [MKMapItem] = [MKMapItem]()
    var point: [MKPointAnnotation] = [MKPointAnnotation]()
    var posicion = CLLocation()
    var selectedAnnotation: MKPointAnnotation!
    
    fileprivate let gps = CLLocationManager()
    fileprivate let request = MKLocalSearchRequest()
    fileprivate var restaurante = true
    fileprivate var ubicacion = true
    //----
    
    override func viewDidLoad() {
        self.navigationController?.isNavigationBarHidden = false
        super.viewDidLoad()
        self.configurarMapa()
        
        //let homeModel = HomeModel()
        //homeModel.delegate = self
        //homeModel.downloadItems(LabelText!)
    }
    
    //Mapa
    fileprivate func configurarMapa() {
        self.mapa.delegate = self
        self.gps.delegate = self
        gps.desiredAccuracy = kCLLocationAccuracyBest
        gps.requestWhenInUseAuthorization()
        let centro = CLLocationCoordinate2DMake(19.59, -99.22)
        let span = MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01)
        let region = MKCoordinateRegionMake(centro, span)
        mapa.region = region
        
        if(FIRApp.defaultApp() == nil){
            FIRApp.configure()
        }
        
        FIRAuth.auth()!.addStateDidChangeListener() { (auth, user) in
            if let user = user {
                print("User is signed in with uid:", user.uid)
            }
        }
        
        let rootRef = FIRDatabase.database().reference()
        
        rootRef.queryOrdered(byChild: "Tiendas").observeSingleEvent(of: .value, with: { (snapshot) in
            if snapshot.exists() {
                let userRef = rootRef.child("Tiendas")
                
                userRef.observe(.childAdded, with: {(snapshot) in
                    
                    let currency = (snapshot.value as? NSDictionary)
                    let latitud = currency!["latitud"] as! Double
                    let longitud = currency!["longitud"] as! Double
                    let nombre = currency!["nombre"] as! String
                    let calle = currency!["calle"] as! String
                    let colonia = currency!["colonia"] as! String
                    let numero = currency!["numero"]
                    
                    var subtitle = "Dirección: Calle: " + calle
                    subtitle = subtitle + " Colonia: " + colonia + " Número: " + (numero! as AnyObject).description
                    let ann = CustomPointAnnotation()
                    ann.coordinate.latitude = Double(latitud)
                    ann.coordinate.longitude = Double(longitud)
                    ann.title = nombre
                    ann.subtitle = subtitle
                    
                    if currency!["promociones"] == nil {
                        ann.imageName = "oso.png"
                    } else {
                        ann.imageName = "osoPromociones.png"
                    }
                    self.point.append(ann)
                    self.mapa.addAnnotation(ann)
                    
                })
            }
        })
    }
    
    func mapView(_ mapView: MKMapView, didUpdate userLocation: MKUserLocation) {
        posicion = userLocation.location!
        mapa.setCenter(posicion.coordinate, animated: true)
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if !(annotation is CustomPointAnnotation) {
            return nil
        }
        
        let reuseId = "test"
        
        var anView = mapView.dequeueReusableAnnotationView(withIdentifier: reuseId)
        
        if anView == nil {
            anView = MKAnnotationView(annotation: annotation, reuseIdentifier: reuseId)
            anView!.canShowCallout = true
            //anView?.rightCalloutAccessoryView = UIButton(type: .DetailDisclosure)
        }
        else {
            anView!.annotation = annotation
        }
        
        //Set annotation-specific properties **AFTER**
        //the view is dequeued or created...
        
        //THIS IS THE GOOD BIT
        let subtitleView = UILabel()
        subtitleView.font = subtitleView.font.withSize(12)
        subtitleView.numberOfLines = 0
        subtitleView.text = annotation.subtitle!
        
        if #available(iOS 9.0, *) {
            anView!.detailCalloutAccessoryView = subtitleView
        } else {
            // Fallback on earlier versions
        }
        
        //let button = UIButton(type: .DetailDisclosure) as UIView // button with info sign in it
        
        let rightButton = UIButton(type: UIButtonType.detailDisclosure);
        anView!.rightCalloutAccessoryView = rightButton;
        
        let cpa = annotation as! CustomPointAnnotation
        anView!.image = UIImage(named:cpa.imageName)
        
        return anView
    }
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        if control == view.rightCalloutAccessoryView {
            
            if control == view.rightCalloutAccessoryView {
                print("Disclosure Pressed! \(view.annotation?.subtitle)")
                let capital = view.annotation
                let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
                let destination = storyboard.instantiateViewController(withIdentifier: "promosTienda") as! promosTienda
                destination.Nombre = ((view.annotation?.title)!)!
                destination.Direccion = ((view.annotation?.subtitle)!)!
                destination.Correo = self.Correo
                self.navigationController?.pushViewController(destination, animated: true)
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //--
    
    /*
     func itemsDownloaded(items: NSArray) {
     print(items)
     feedItems = items
     }
     
     func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
     // Return the number of feed items
     return feedItems.count
     }
     
     func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
     // Retrieve cell
     let cellIdentifier: String = "BasicCell"
     let myCell: UITableViewCell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier)!
     // Get the location to be shown
     let item: LocationModel = feedItems[indexPath.row] as! LocationModel
     
     // Get references to labels of cell
     myCell.textLabel!.text = item.platillo
     
     if restaurante {
     matchingItems.removeAll()
     
     let ann = MKPointAnnotation()
     ann.coordinate.latitude = Double(item.cantidad!)!
     ann.coordinate.longitude = Double(item.precio!)!
     ann.title = item.platillo
     self.point.append(ann)
     self.mapa.addAnnotation(ann)
     }
     return myCell
     }
     */
    
}
