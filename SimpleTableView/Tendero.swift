//
//  Tendero.swift
//  SimpleTableView
//
//  Created by Sebastián Loredo on 25/09/16.
//  Copyright © 2016 Sebastián Loredo. All rights reserved.
//

import Foundation
import UIKit
import Firebase
import SpriteKit

class Tendero: UIViewController {
    @IBOutlet var tableView: UITableView!
    
    var opcion: Int = 0
    var campo = ""
    var items: [String] = ["Ver nuevas promociones", "Mis promociones", "Cupones canjeados", "Clientes leales", "Quejas y Sugerencias"]
    var Datos = ""
    var Nombre = ""
    var Codigo:Int = 0
    var timer = Timer()
    var arrPromociones: [String] = []
    var arrSolicitudesCupon: [String] = []
    
    @IBOutlet weak var saludo: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        scheduledTimerWithTimeInterval()
        saludo.text = Nombre
        //tableView.delegate = self
        //tableView.dataSource = self
        //self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
    }
    
    func scheduledTimerWithTimeInterval() {
        // Scheduling timer to Call the function **Countdown** with the interval of 1 seconds
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.revisarSolicitud), userInfo: nil, repeats: true)
    }
    
    func revisarSolicitud() {
        
        if(FIRApp.defaultApp() == nil){
            FIRApp.configure()
        }
        
        FIRAuth.auth()!.addStateDidChangeListener() { (auth, user) in
            if let user = user {
                print("User is signed in with uid:", user.uid)
            }
        }
        
        let rootRef = FIRDatabase.database().reference()
        
        let userRef = rootRef.child("Tiendas")
        
        userRef.observe(.childAdded, with: {(snapshot) in
            if snapshot.exists() {
                let currency = (snapshot.value as? NSDictionary) //316641
                let codigo = currency?["codigo"] as! Int
                let key = snapshot.key
                
                if (codigo == self.Codigo) {
                    
                    let solicitudes = currency?["solicitudes"] as? String ?? ""
                    
                    self.arrPromociones = solicitudes.characters.split{$0 == ","}.map(String.init)
                    
                    if self.arrPromociones.count > 0 {
                        let refreshAlert = UIAlertController(title: "Refresh", message: "Aceptar la solicitud?", preferredStyle: UIAlertControllerStyle.alert)
                        
                        refreshAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
                            
                            var amigos = currency!["amigos"] as? String ?? ""
                            let arrPromociones1 = amigos.characters.split{$0 == ","}.map(String.init)
                            var dict = [String:Int]()
                            
                            for i in arrPromociones1 {
                                let holi = i.characters.split{$0 == ":"}.map(String.init)
                                dict[holi[0]] = Int(holi[1])!
                            }

                            if dict.index(forKey: self.arrPromociones[0]) != nil {
                                for (key, value) in dict {
                                    if key == self.arrPromociones[0] {
                                        dict[self.arrPromociones[0]] = value + 1
                                    }
                                }
                            } else {
                                dict[self.arrPromociones[0]] = 0
                            }
                            
                            var text = ""
                            for (key, value) in dict {
                                text = text + key + ":" + String(value) + ","
                            }
                            
                            rootRef.child("Tiendas").child(key).updateChildValues(["amigos": text])
                            
                            let newText = ""
                            
                            rootRef.child("Tiendas").child(key).updateChildValues(["solicitudes": newText])
                            
                            let userRef = rootRef.child("clientes")
                            
                            userRef.observe(.childAdded, with: {(snapshot) in
                                if snapshot.exists() {
                                    let currency = (snapshot.value as? NSDictionary) //316641
                                    let correo = currency?["correo"] as! String
                                    let key = snapshot.key
                                    let arrPromociones1 = amigos.characters.split{$0 == ","}.map(String.init)
                                    print("holi")
                                    if (correo == self.arrPromociones[0]) {
                                        
                                        var amigos = currency!["amigos"] as? String ?? ""
                                        amigos = amigos + String(self.Codigo)
                                        print(String(self.Codigo))
                                        print("holi2")
                                        let arrPromociones2 = amigos.characters.split{$0 == ","}.map(String.init)
                                        
                                        var listaNueva: [String] = []
                                        
                                        for i in arrPromociones2 {
                                            if listaNueva.contains(i) {
                                                
                                            } else {
                                                listaNueva.append(i)
                                            }
                                        }
                                        
                                        var text = ""
                                        
                                        for i in listaNueva {
                                            text = text + i + ","
                                        }
                                        
                                        rootRef.child("clientes").child(key).updateChildValues(["amigos": text])
                                    }
                                }
                            })
                            self.scheduledTimerWithTimeInterval()
                        }))
 
                        refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action: UIAlertAction!) in
                            let newText = ""
                            
                            rootRef.child("Tiendas").child(key).updateChildValues(["solicitudes": newText])
                            self.scheduledTimerWithTimeInterval()
                            
                        }))
                        
                        self.present(refreshAlert, animated: true, completion: nil)
                    }
                }
            }
        })
        
        let userRef2 = rootRef.child("Tiendas")
        
        userRef2.observe(.childAdded, with: {(snapshot) in
            if snapshot.exists() {
                let currency = (snapshot.value as? NSDictionary) //316641
                let codigo = currency?["codigo"] as! Int
                let key = snapshot.key
                
                if (codigo == self.Codigo) {
                    
                    let solicitudes = currency?["solicitudesCupon"] as? String ?? ""
                    
                    self.arrSolicitudesCupon = solicitudes.characters.split{$0 == ","}.map(String.init)
                    
                    if self.arrSolicitudesCupon.count > 0 {
                        let refreshAlert = UIAlertController(title: "Refresh", message: "Aceptar la solicitud?", preferredStyle: UIAlertControllerStyle.alert)
                        
                        refreshAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
                            
                            var cuponesCanjeados = currency!["cuponesCanjeados"] as? String ?? ""
                            if (self.arrSolicitudesCupon.count == 0) {
                                self.scheduledTimerWithTimeInterval()
                            } else {
                                cuponesCanjeados = cuponesCanjeados + self.arrSolicitudesCupon[0]
                                
                                let arrPromociones1 = cuponesCanjeados.characters.split{$0 == ","}.map(String.init)
                                
                                print(arrPromociones1)
                                let arrDesgloce = arrPromociones1.last?.characters.split{$0 == ":"}.map(String.init)
                                
                                var text = ""
                                print(arrDesgloce)
                                
                                
                                for i in arrPromociones1 {
                                    text = text + i.characters.split{$0 == ":"}.map(String.init)[0] + ","
                                }
                                
                                rootRef.child("Tiendas").child(key).updateChildValues(["cuponesCanjeados": text])
                                
                                let newText = ""
                                
                                rootRef.child("Tiendas").child(key).updateChildValues(["solicitudesCupon": newText])
                                
                                let userRef = rootRef.child("clientes")
                                
                                userRef.observe(.childAdded, with: {(snapshot) in
                                    if snapshot.exists() {
                                        let currency = (snapshot.value as? NSDictionary) //316641
                                        let correo = currency?["correo"] as! String
                                        let key = snapshot.key
                                        
                                        if (correo == arrDesgloce?[1]) {
                                            
                                            var cupones = currency!["cupones"] as? String ?? ""
                                            
                                            var arrPromociones2 = cupones.characters.split{$0 == ","}.map(String.init)
                                            
                                            var listaNueva: [String] = []
                                            
                                            if let index = arrPromociones2.index(of: arrDesgloce![0]) {
                                                arrPromociones2.remove(at: index)
                                            }
                                            
                                            var text = ""
                                            
                                            for i in arrPromociones2 {
                                                text = text + i + ","
                                            }
                                            
                                            rootRef.child("clientes").child(key).updateChildValues(["cupones": text])
                                            
                                        }
                                    }
                                })
                                
                                self.scheduledTimerWithTimeInterval()
                            }
                            
                            
                            
                        }))
                        
                        refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action: UIAlertAction!) in
                            let newText = ""
                            
                            rootRef.child("Tiendas").child(key).updateChildValues(["solicitudesCupon": newText])
                            self.scheduledTimerWithTimeInterval()
                            
                        }))
                        
                        self.present(refreshAlert, animated: true, completion: nil)
                    }
                }
            }
        })
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.items.count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:UITableViewCell = self.tableView.dequeueReusableCell(withIdentifier: "cell")! as UITableViewCell
        
        cell.textLabel?.text = self.items[(indexPath as NSIndexPath).row]
        
        return cell
    }
    
    @IBAction func nuevasPromos(_ sender: AnyObject) {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let destination = storyboard.instantiateViewController(withIdentifier: "nuevasPromociones") as! nuevasPromociones
        destination.Nombre = Nombre
        destination.Codigo = Codigo
        navigationController?.pushViewController(destination, animated: true)
    }
    
    @IBAction func misPromos(_ sender: AnyObject) {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let destination = storyboard.instantiateViewController(withIdentifier: "MisPromociones") as! MisPromociones
        destination.Nombre = Nombre
        destination.Codigo = Codigo
        navigationController?.pushViewController(destination, animated: true)
    }
    
    @IBAction func cuponesCanjeados(_ sender: AnyObject) {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let destination = storyboard.instantiateViewController(withIdentifier: "CuponesCanje") as! CuponesCanje
        destination.Codigo = Codigo
        navigationController?.pushViewController(destination, animated: true)
    }
    
    @IBAction func clientesLeales(_ sender: AnyObject) {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let destination = storyboard.instantiateViewController(withIdentifier: "ClientesLeales") as! ClientesLeales
        destination.Codigo = Codigo
        destination.Nombre = Nombre
        navigationController?.pushViewController(destination, animated: true)
    }
    
    @IBAction func contacto(_ sender: AnyObject) {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let destination = storyboard.instantiateViewController(withIdentifier: "ComentarioTendero") as! ComentarioTendero
        destination.Nombre = Nombre
        destination.Codigo = Codigo
        navigationController?.pushViewController(destination, animated: true)
    }
}
