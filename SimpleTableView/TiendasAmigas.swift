//
//  TiendasAmigas.swift
//  SimpleTableView
//
//  Created by Sebastián Loredo on 24/10/16.
//  Copyright © 2016 Sebastián Loredo. All rights reserved.
//

import Foundation
import UIKit
import Firebase

class TiendasAmigas: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    var Cupon = ""
    var Correo = ""
    var items = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        
        if(FIRApp.defaultApp() == nil){
            FIRApp.configure()
        }
        
        FIRAuth.auth()!.addStateDidChangeListener() { (auth, user) in
            if let user = user {
                print("User is signed in with uid:", user.uid)
            }
        }
        
        let rootRef = FIRDatabase.database().reference()
        
        let userRef = rootRef.child("clientes")
        
        userRef.observe(.childAdded, with: {(snapshot) in
            if snapshot.exists() {
                let currency = (snapshot.value as? NSDictionary)
                
                let correo = currency?["correo"] as! String
                if (correo == self.Correo) {
                    let amigos = currency?["amigos"] as? String ?? ""
                    let arrAmigos = amigos.characters.split{$0 == ","}.map(String.init)
                    
                    let userRef2 = rootRef.child("Tiendas")
                    var nombres = [String]()
                    
                    userRef2.observe(.childAdded, with: {(snapshot) in
                        if snapshot.exists() {
                            let currency = (snapshot.value as? NSDictionary)
                            
                            let codigo = currency?["codigo"] as! Int
                            for i in arrAmigos {
                                if (codigo == Int(i)) {
                                    let nombre = currency?["nombre"] as! String
                                    nombres.append(nombre)
                                }
                            }
                        }
                        self.items = nombres
                        self.tableView.reloadData()
                    })
                }
            }
        })
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.items.count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:UITableViewCell = self.tableView.dequeueReusableCell(withIdentifier: "cell")! as UITableViewCell
        cell.backgroundColor = UIColor.clear

        cell.textLabel?.text = self.items[(indexPath as NSIndexPath).row]
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var tienda = self.items[(indexPath as NSIndexPath).row]
        if(FIRApp.defaultApp() == nil){
            FIRApp.configure()
        }
        
        FIRAuth.auth()!.addStateDidChangeListener() { (auth, user) in
            if let user = user {
                print("User is signed in with uid:", user.uid)
            }
        }
        
        let rootRef = FIRDatabase.database().reference()
        
        let userRef = rootRef.child("Tiendas")
        
        userRef.observe(.childAdded, with: {(snapshot) in
            if snapshot.exists() {
                
                let currency = (snapshot.value as? NSDictionary)
                let nombre = currency!["nombre"] as! String
                let key = snapshot.key
                if (nombre == tienda) {
                    
                    var solicitudesCupon = currency!["solicitudesCupon"] as? String ?? ""
                    solicitudesCupon = solicitudesCupon + self.Cupon + ":" + self.Correo
                    
                    let arrsolicitudesCupon = solicitudesCupon.characters.split{$0 == ","}.map(String.init)
                    
                    var listaNueva: [String] = []
                    
                    for i in arrsolicitudesCupon {
                        if listaNueva.contains(i) {
                            
                        } else {
                            listaNueva.append(i)
                        }
                    }
                    
                    var text = ""
                    
                    for i in listaNueva {
                        text = text + i + ","
                    }
                    
                    rootRef.child("Tiendas").child(key).updateChildValues(["solicitudesCupon": text])
                    let alert = UIAlertView()
                    alert.title = "Listo"
                    alert.message = "Solicitud enviada, espera a que el Tendero te responda."
                    alert.addButton(withTitle: "Ok")
                    alert.show()
                    
                    let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
                    let destination = storyboard.instantiateViewController(withIdentifier: "Main") as! Main
                    destination.Correo = self.Correo
                    self.navigationController?.pushViewController(destination, animated: true)
                    
                }
            }
        })
    }
}
