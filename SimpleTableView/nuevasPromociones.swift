//
//  nuevasPromociones.swift
//  SimpleTableView
//
//  Created by Sebastián Loredo on 09/10/16.
//  Copyright © 2016 Sebastián Loredo. All rights reserved.
//

import Foundation
import UIKit
import Firebase

class nuevasPromociones: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet var tableView: UITableView!
    @IBOutlet weak var listaPromociones: UILabel!
    
    var opcion: Int = 0
    var campo = ""
    var producto: [String] = []
    var promocion: [String] = []
    var indices: [Int] = []
    var Datos = ""
    var Nombre = ""
    var Codigo:Int = 0
    var timer = Timer()
    var arrPromociones: [String] = []
    var arrSolicitudesCupon: [String] = []
    
    override func viewDidLoad() {
        scheduledTimerWithTimeInterval()
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        
        if(FIRApp.defaultApp() == nil){
            FIRApp.configure()
        }
        
        FIRAuth.auth()!.addStateDidChangeListener() { (auth, user) in
            if let user = user {
                print("User is signed in with uid:", user.uid)
            }
        }
        
        let rootRef = FIRDatabase.database().reference()
        
        let userRef = rootRef.child("promociones")
        
        userRef.queryOrdered(byChild: "producto").observe(.value, with: { snapshot in
            if snapshot.exists() {
                let currency = snapshot.value! as! NSDictionary
                
                for (_, value) in currency {
                    let value2 = value as! NSDictionary
                    for (key, value) in value2 {
                        if key as! String == "producto" {
                            self.producto.append(value as! String)
                        } else {
                            self.promocion.append(value as! String)
                        }
                    }
                }
                self.tableView.reloadData()
            }
        })
        
        super.viewDidLoad()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.producto.count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .subtitle, reuseIdentifier: "celda")
        
        cell.textLabel?.text = producto[(indexPath as NSIndexPath).row]
        cell.backgroundColor = UIColor.clear
        cell.detailTextLabel?.text = promocion[(indexPath as NSIndexPath).row]
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var t = 0
        if indices.contains((indexPath as NSIndexPath).row) {
            for i in indices {
                if i == (indexPath as NSIndexPath).row {
                    indices.remove(at: indices.index(of: (indexPath as NSIndexPath).row)!)
                    t = 1
                }
            }
            listaPromociones.text = ""
            for i in indices {
                listaPromociones.text = listaPromociones.text! + " " + producto[i]
                    + "(" + promocion[i] + "),"
            }
        } else {
            listaPromociones.text = listaPromociones.text! + " " + producto[(indexPath as NSIndexPath).row]
                + "(" + promocion[(indexPath as NSIndexPath).row] + "),"
        }
        if t == 0 {
            indices.append((indexPath as NSIndexPath).row)
        }
    }
    
    @IBAction func aceptar(_ sender: AnyObject) {
        if indices.count == 0 {
            let alert = UIAlertView()
            alert.title = "Error"
            alert.message = "Debes elegir al menos una promoción. Si no quieres elegir una presiona Bienvenido para volver al menú"
            alert.addButton(withTitle: "Ok")
            alert.show()
        } else {
            if(FIRApp.defaultApp() == nil){
                FIRApp.configure()
            }
            
            FIRAuth.auth()!.addStateDidChangeListener() { (auth, user) in
                if let user = user {
                    print("User is signed in with uid:", user.uid)
                }
            }
            
            let rootRef = FIRDatabase.database().reference()
            
            let userRef = rootRef.child("Tiendas")
            
            userRef.observe(.childAdded, with: {(snapshot) in
                if snapshot.exists() {
                    
                    let currency = (snapshot.value as? NSDictionary)
                    let codigo = currency!["codigo"] as! Int
                    let key = snapshot.key
                    if (codigo == self.Codigo) {
                        var promociones = currency!["promociones"] as? String ?? ""
                        promociones = promociones + self.listaPromociones.text!
                        
                        let arrPromociones = promociones.characters.split{$0 == ","}.map(String.init)
                        
                        var listaNueva: [String] = []
                        
                        for i in arrPromociones {
                            if listaNueva.contains(i) {
                                
                            } else {
                                listaNueva.append(i)
                            }
                        }
                        
                        var text = ""
                        
                        for i in listaNueva {
                            text = text + i + ","
                        }
                        
                        print(text)
                        rootRef.child("Tiendas").child(key).updateChildValues(["promociones": text])
                        let alert = UIAlertView()
                        alert.title = "Listo"
                        alert.message = "Las promociones están disponibles para todo el público"
                        alert.addButton(withTitle: "Ok")
                        alert.show()
                    }
                }
            })
            
        }
    }
    
    func scheduledTimerWithTimeInterval() {
        // Scheduling timer to Call the function **Countdown** with the interval of 1 seconds
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.revisarSolicitud), userInfo: nil, repeats: true)
    }
    
    func revisarSolicitud() {
        
        if(FIRApp.defaultApp() == nil){
            FIRApp.configure()
        }
        
        FIRAuth.auth()!.addStateDidChangeListener() { (auth, user) in
            if let user = user {
                print("User is signed in with uid:", user.uid)
            }
        }
        
        let rootRef = FIRDatabase.database().reference()
        
        let userRef = rootRef.child("Tiendas")
        
        userRef.observe(.childAdded, with: {(snapshot) in
            if snapshot.exists() {
                let currency = (snapshot.value as? NSDictionary) //316641
                let codigo = currency?["codigo"] as! Int
                let key = snapshot.key
                
                if (codigo == self.Codigo) {
                    
                    let solicitudes = currency?["solicitudes"] as? String ?? ""
                    
                    self.arrPromociones = solicitudes.characters.split{$0 == ","}.map(String.init)
                    
                    if self.arrPromociones.count > 0 {
                        let refreshAlert = UIAlertController(title: "Refresh", message: "Aceptar la solicitud?", preferredStyle: UIAlertControllerStyle.alert)
                        
                        refreshAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
                            
                            var amigos = currency!["amigos"] as? String ?? ""
                            amigos = amigos + self.arrPromociones[0]
                            
                            let arrPromociones1 = amigos.characters.split{$0 == ","}.map(String.init)
                            
                            var listaNueva: [String] = []
                            
                            for i in arrPromociones1 {
                                if listaNueva.contains(i) {
                                    
                                } else {
                                    listaNueva.append(i)
                                }
                            }
                            
                            var text = ""
                            
                            for i in listaNueva {
                                text = text + i + ","
                            }
                            
                            rootRef.child("Tiendas").child(key).updateChildValues(["amigos": text])
                            
                            let newText = ""
                            
                            rootRef.child("Tiendas").child(key).updateChildValues(["solicitudes": newText])
                            
                            let userRef = rootRef.child("clientes")
                            
                            userRef.observe(.childAdded, with: {(snapshot) in
                                if snapshot.exists() {
                                    let currency = (snapshot.value as? NSDictionary) //316641
                                    let correo = currency?["correo"] as! String
                                    let key = snapshot.key
                                    let arrPromociones1 = amigos.characters.split{$0 == ","}.map(String.init)
                                    
                                    if (correo == arrPromociones1[0]) {
                                        
                                        var amigos = currency!["amigos"] as? String ?? ""
                                        amigos = amigos + String(self.Codigo)
                                        
                                        let arrPromociones2 = amigos.characters.split{$0 == ","}.map(String.init)
                                        
                                        var listaNueva: [String] = []
                                        
                                        for i in arrPromociones2 {
                                            if listaNueva.contains(i) {
                                                
                                            } else {
                                                listaNueva.append(i)
                                            }
                                        }
                                        
                                        var text = ""
                                        
                                        for i in listaNueva {
                                            text = text + i + ","
                                        }
                                        
                                        rootRef.child("clientes").child(key).updateChildValues(["amigos": text])
                                    }
                                }
                            })
                            self.scheduledTimerWithTimeInterval()
                            
                        }))
                        
                        refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action: UIAlertAction!) in
                            let newText = ""
                            
                            rootRef.child("Tiendas").child(key).updateChildValues(["solicitudes": newText])
                            self.scheduledTimerWithTimeInterval()
                            
                        }))
                        
                        self.present(refreshAlert, animated: true, completion: nil)
                    }
                }
            }
        })
        
        let userRef2 = rootRef.child("Tiendas")
        
        userRef2.observe(.childAdded, with: {(snapshot) in
            if snapshot.exists() {
                let currency = (snapshot.value as? NSDictionary) //316641
                let codigo = currency?["codigo"] as! Int
                let key = snapshot.key
                
                if (codigo == self.Codigo) {
                    
                    let solicitudes = currency?["solicitudesCupon"] as? String ?? ""
                    
                    self.arrSolicitudesCupon = solicitudes.characters.split{$0 == ","}.map(String.init)
                    
                    if self.arrSolicitudesCupon.count > 0 {
                        let refreshAlert = UIAlertController(title: "Refresh", message: "Aceptar la solicitud?", preferredStyle: UIAlertControllerStyle.alert)
                        
                        refreshAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
                            
                            var cuponesCanjeados = currency!["cuponesCanjeados"] as? String ?? ""
                            cuponesCanjeados = cuponesCanjeados + self.arrSolicitudesCupon[0]
                            
                            let arrPromociones1 = cuponesCanjeados.characters.split{$0 == ","}.map(String.init)
                            
                            print(arrPromociones1)
                            let arrDesgloce = arrPromociones1.last?.characters.split{$0 == ":"}.map(String.init)
                            
                            var text = ""
                            print(arrDesgloce)
                            
                            
                            for i in arrPromociones1 {
                                text = text + i.characters.split{$0 == ":"}.map(String.init)[0] + ","
                            }
                            
                            rootRef.child("Tiendas").child(key).updateChildValues(["cuponesCanjeados": text])
                            
                            let newText = ""
                            
                            rootRef.child("Tiendas").child(key).updateChildValues(["solicitudesCupon": newText])
                            
                            let userRef = rootRef.child("clientes")
                            
                            userRef.observe(.childAdded, with: {(snapshot) in
                                if snapshot.exists() {
                                    let currency = (snapshot.value as? NSDictionary) //316641
                                    let correo = currency?["correo"] as! String
                                    let key = snapshot.key
                                    
                                    if (correo == arrDesgloce?[1]) {
                                        
                                        var cupones = currency!["cupones"] as? String ?? ""
                                        
                                        var arrPromociones2 = cupones.characters.split{$0 == ","}.map(String.init)
                                        
                                        var listaNueva: [String] = []
                                        
                                        if let index = arrPromociones2.index(of: arrDesgloce![0]) {
                                            arrPromociones2.remove(at: index)
                                        }
                                        
                                        var text = ""
                                        
                                        for i in arrPromociones2 {
                                            text = text + i + ","
                                        }
                                        
                                        rootRef.child("clientes").child(key).updateChildValues(["cupones": text])
                                        
                                    }
                                }
                            })
                            
                            self.scheduledTimerWithTimeInterval()
                            
                        }))
                        
                        refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action: UIAlertAction!) in
                            let newText = ""
                            
                            rootRef.child("Tiendas").child(key).updateChildValues(["solicitudesCupon": newText])
                            self.scheduledTimerWithTimeInterval()
                            
                        }))
                        
                        self.present(refreshAlert, animated: true, completion: nil)
                    }
                }
            }
        })
    }
}
