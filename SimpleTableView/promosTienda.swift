//
//  promosTienda.swift
//  SimpleTableView
//
//  Created by Sebastián Loredo on 07/10/16.
//  Copyright © 2016 Sebastián Loredo. All rights reserved.
//

import Foundation
import UIKit
import Firebase
import SystemConfiguration
import Firebase

class promosTienda: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var lblNombre: UILabel!
    var Nombre = ""
    var Direccion = ""
    var Codigo:Int = 0
    var Correo = ""
    var items: [String] = []
    var indices: [Int] = []
    
    override func viewDidLoad() {
        lblNombre.text = Nombre
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        
        let separators = NSCharacterSet(charactersIn: ": ")
        var words = Direccion.components(separatedBy: separators as CharacterSet)
        
        if(FIRApp.defaultApp() == nil){
            FIRApp.configure()
        }
        
        FIRAuth.auth()!.addStateDidChangeListener() { (auth, user) in
            if let user = user {
                print("User is signed in with uid:", user.uid)
            }
        }
        
        let rootRef = FIRDatabase.database().reference()
        
        let userRef = rootRef.child("Tiendas")
        
        userRef.observe(.childAdded, with: {(snapshot) in
            if snapshot.exists() {
                let currency = (snapshot.value as? NSDictionary)
                
                let nombre = currency?["nombre"] as? String ?? ""
                let calle = currency?["calle"] as? String ?? ""
                let codigo = currency?["codigo"]
                
                if (nombre == self.Nombre && calle as? String ?? "" == words[4] as? String ?? "") {
                    
                    let promociones = currency?["promociones"] as? String ?? ""
                    let arrPromociones = promociones.characters.split{$0 == ","}.map(String.init)
                    self.lblNombre.text = "Tienda " + self.Nombre
                    self.Codigo = codigo as! Int
                    print(" codigo")
                    
                    print(self.Codigo)
                    
                    self.items = arrPromociones
                    self.tableView.reloadData()
                }
            }
        })
    print(lblNombre.text)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.items.count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:UITableViewCell = self.tableView.dequeueReusableCell(withIdentifier: "cell")! as UITableViewCell
        cell.backgroundColor = UIColor.clear
        cell.textLabel?.text = self.items[(indexPath as NSIndexPath).row]
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

    }
    
    @IBOutlet weak var btnAgregar: UIButton!
    @IBAction func btnAmigo(_ sender: AnyObject) {
        if(FIRApp.defaultApp() == nil){
            FIRApp.configure()
        }
        
        FIRAuth.auth()!.addStateDidChangeListener() { (auth, user) in
            if let user = user {
                print("User is signed in with uid:", user.uid)
            }
        }
        
        let rootRef = FIRDatabase.database().reference()
        
        let userRef = rootRef.child("Tiendas")
        
        userRef.observe(.childAdded, with: {(snapshot) in
            if snapshot.exists() {
                
                let currency = (snapshot.value as? NSDictionary)
                let codigo = currency!["codigo"] as! Int
                let key = snapshot.key
                if (codigo == self.Codigo) {
                    
                    var promociones = currency!["solicitudes"] as? String ?? ""
                    promociones = promociones + self.Correo
                    
                    let arrPromociones = promociones.characters.split{$0 == ","}.map(String.init)
                    
                    var listaNueva: [String] = []
                    
                    for i in arrPromociones {
                        if listaNueva.contains(i) {
                            
                        } else {
                            listaNueva.append(i)
                        }
                    }
                    
                    var text = ""
                    
                    for i in listaNueva {
                        text = text + i + ","
                    }
                    
                    rootRef.child("Tiendas").child(key).updateChildValues(["solicitudes": text])
                    let alert = UIAlertView()
                    alert.title = "Listo"
                    alert.message = "Solicitud enviada"
                    alert.addButton(withTitle: "Ok")
                    alert.show()
                    self.btnAgregar.isEnabled = false
                }
            }
        })
    }
    
}
